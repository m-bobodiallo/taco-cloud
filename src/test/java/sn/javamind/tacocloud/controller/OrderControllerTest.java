package sn.javamind.tacocloud.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import sn.javamind.tacocloud.model.TacoOrder;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(OrderController.class)
class OrderControllerTest {

    private TacoOrder order;

    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    public void setUp() {
        order = TacoOrder.builder()
                .name("Order1")
                .street("Rue 1")
                .city("Dakar")
                .state("Dakar")
                .zip("11500")
                .ccNumber("371449635398431")
                .ccExpiration("12/21")
                .ccCVV("989")
                .build();
    }

    @Test
    public void getOrderForm_shouldReturnAForm() throws Exception {
        mockMvc.perform(get("/orders/current"))
                .andExpect(status().isOk())
                .andExpect(view().name("orderForm"))
                .andExpect(content().string(containsString("Order your taco creations!")));
    }

    @Test
    public void processOrder_shouldReturnOrderFormViewIfFormIsInvalid() throws Exception {
        mockMvc.perform(post("/orders"))
                .andExpect(view().name("orderForm"));
    }

    @Test
    public void processOrder_shouldRedirectToHomePageIfFormIsValid() throws Exception {
        mockMvc.perform(
                post("/orders")
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                        .param("name", order.getName())
                        .param("street", order.getStreet())
                        .param("city", order.getCity())
                        .param("state", order.getState())
                        .param("zip", order.getZip())
                        .param("ccNumber", order.getCcNumber())
                        .param("ccExpiration", order.getCcExpiration())
                        .param("ccCVV", order.getCcCVV()))
                .andExpect(status().is3xxRedirection())
                .andExpect(header().string("Location", "/"));
    }
}
