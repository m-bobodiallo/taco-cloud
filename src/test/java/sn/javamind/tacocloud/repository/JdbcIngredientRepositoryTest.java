package sn.javamind.tacocloud.repository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DuplicateKeyException;
import sn.javamind.tacocloud.model.Ingredient;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
class JdbcIngredientRepositoryTest {

    @Autowired
    private IngredientRepository ingredientRepository;

    private Ingredient ingredient;
    private Ingredient existIngredient;

    @BeforeEach
    public void setUp() {
        ingredient = new Ingredient("CART", "Carnitas", Ingredient.Type.PROTEIN);
        existIngredient = new Ingredient("CARN", "Carnitas", Ingredient.Type.PROTEIN);
    }

    @Test
    public void shouldSaveIngredient() {
        Ingredient ingredientSaved = ingredientRepository.save(ingredient);
        assertThat(ingredientSaved.getId()).isEqualTo("CART");
    }

    @Test
    public void saveIngredient_shouldThrowExceptionWhenIdExist() {
        assertThrows(DuplicateKeyException.class, () -> ingredientRepository.save(existIngredient));
    }

    @Test
    public void shouldFindIngredientById() {
        Optional<Ingredient> find = ingredientRepository.findById(existIngredient.getId());

        assertThat(find).isPresent();
        assertThat(find.get())
                .hasFieldOrPropertyWithValue("id", existIngredient.getId())
                .hasFieldOrPropertyWithValue("name", existIngredient.getName())
                .hasFieldOrPropertyWithValue("type", existIngredient.getType());
    }

    public void shouldRetreiveAllIngredients() {
        Iterable<Ingredient> ingredients = ingredientRepository.findAll();

    }

}
