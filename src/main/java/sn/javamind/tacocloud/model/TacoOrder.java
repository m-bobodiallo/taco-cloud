package sn.javamind.tacocloud.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.CreditCardNumber;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TacoOrder implements Serializable {
    private static final long serialVersionUID = 1L;

    @NotBlank(message = "Name is requited")
    private String name;

    @NotBlank(message = "Street is requited")
    private String street;

    @NotBlank(message = "City is requited")
    private String city;

    @NotBlank(message = "State is requited")
    private String state;

    @NotBlank(message = "Zip code is requited")
    private String zip;

    @CreditCardNumber(message = "Not a valid credit card number")
    private String ccNumber;

    @Pattern(regexp = "^(0[1-9]|1[0-9])([\\/])([1-9][0-9])$", message = "Must be formatted MM/YY")
    private String ccExpiration;

    @Digits(integer = 3, fraction = 0, message = "Invalid CVV")
    private String ccCVV;

    private Date placedAt;

}
