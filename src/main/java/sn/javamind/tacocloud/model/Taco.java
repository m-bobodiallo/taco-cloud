package sn.javamind.tacocloud.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Taco {

    private Long id;

    @NotNull(message = "Name must not ne null")
    @Size(min = 5, message = "Name must be at at least 5 characters long")
    private String name;

    @Size(min = 1, message = "You must use a least 1 ingredient")
    private List<String> ingredients;

    private Date createdAt = new Date();
}
