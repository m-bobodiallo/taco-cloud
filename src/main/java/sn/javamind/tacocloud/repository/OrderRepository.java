package sn.javamind.tacocloud.repository;

import sn.javamind.tacocloud.model.TacoOrder;

public interface OrderRepository {
    TacoOrder save(TacoOrder tacoOrder);
}
